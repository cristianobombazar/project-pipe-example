import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nome = 'Cristiano Bombazar';
  dataAniversario = new Date();
  preco = 10.312;
  troco = 12.123;
}
